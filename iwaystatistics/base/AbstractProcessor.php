<?php
namespace iwaystatistics\base;

abstract class AbstractProcessor {
	protected $db = null;
	protected $executionId = null;
	
	public function __construct (\Mysqli $db, $executionId) {
		$this->db = $db;
		$this->executionId = $executionId;
	}
	
	public function setDb(\Mysqli $db) {
		$this->db = $db;
		return $this;
	}
	
	public function setExecutionId($executionId){
		$this->executionId=$executionId;
		return $this;
	}
	
}