<?php
namespace iwaystatistics\xunit;
use iwaystatistics\base\AbstractProcessor;
require_once (__DIR__.'/../base/AbstractProcessor.php');
require_once (__DIR__.'/Parser.php');
require_once (__DIR__.'/DatabasePersister.php');

class Processor extends AbstractProcessor{
	
	public function execute($logFilename, $coverageFilename){
		$parser = new \iwaystatistics\xunit\Parser();

		$dataLog = $parser->parseLog($logFilename);
		
		$dataCov = $parser->parseCoverage($coverageFilename);
		
		$data = array(
				'executionId' => $this->executionId,
				'errors' => $dataLog,
				'coverage' => $dataCov
		);
		
		$databasePersister = new \iwaystatistics\xunit\DatabasePersister();
		$databasePersister->setDb($this->db);
		$databasePersister->persist($data);
	}
}
