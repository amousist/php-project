<?php

namespace iwaystatistics\xunit;

use iwaystatistics\base\AbstractDatabasePersister;
require_once (__DIR__ . '/../base/AbstractDatabasePersister.php');
class DatabasePersister extends AbstractDatabasePersister {
	public function persist(array $data) {
		extract ( $data );
		
		$sql = <<<EOQ
INSERT INTO testunitarios (	execution_id,
							total_tests,
							total_ok,
							total_nok,
							total_errors,
							total_statements,
							coverage_statements,
							porcentual_covered_statements) 
VALUES (?, ?, ?, ?, ?, ?, ?, ?)
EOQ;
		$stmt = $this->db->prepare ( $sql );
		
		$stmt->bind_param ( "iiiiiiid",
							$executionId,
							$errors ['total_tests'],
							$errors ['total_ok'],
							$errors ['total_nok'],
							$errors ['total_errors'],
							$coverage ['total_statements'],
							$coverage ['coverage_statements'],
							$coverage ['porcentual_covered_statements'] );
		$stmt->execute ();
		$stmt->close ();
	}
}
