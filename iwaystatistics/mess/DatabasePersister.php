<?php
namespace iwaystatistics\mess;
use iwaystatistics\base\AbstractDatabasePersister;
require_once (__DIR__.'/../base/AbstractDatabasePersister.php');

class DatabasePersister extends AbstractDatabasePersister{
	public function persist(array $data) {
		extract ( $data );
		
		$sql = <<<EOQ
INSERT INTO messmetrics (execution_id, priority, amount)
VALUES (?, ?, ?)
EOQ;
		$stmt = $this->db->prepare ( $sql );
		foreach ( $data as $priority => $priorityType) {
			if ($priorityType ['amount'] != 0){
				$stmt->bind_param ( "iii", $executionId, $priority, $priorityType ['amount'] );
				$stmt->execute ();
			}
		}
		$stmt->close ();
	}
}
