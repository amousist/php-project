<?php

namespace iwaystatistics\phpcompat;

class Parser{
	protected $totalesXversion = array (
			'5.5' => array (
					'warnings' => 0,
					'errors' => 0 
			),
			'5.4' => array (
					'warnings' => 0,
					'errors' => 0 
			),
			'5.3' => array (
					'warnings' => 0,
					'errors' => 0 
			) 
	);
	public function parse($filename) {
		$handle = fopen ( $filename, "r" );
		if ($handle) {
			while ( ($buffer = fgets ( $handle, 4096 )) !== false ) {
				$aux = explode ( ",", $buffer );
				$data = array (
						'file' => trim ( $aux [0] ),
						'line' => ( int ) trim ( $aux [1] ),
						'column' => ( int ) trim ( $aux [2] ),
						'type' => trim ( $aux [3] ),
						'message' => trim ( $aux [4] ),
						'source' => trim ( $aux [5] ) 
				);
				if (strtolower ( $data ['file'] ) == 'file') {
					continue;
				}
				
				$type = ($data ['type'] == 'warning') ? 'warnings' : 'errors';
				if (strpos ( $data ['message'], '5.3' ) !== false) {
					$this->totalesXversion ['5.3'] [$type] ++;
					$this->totalesXversion ['5.4'] [$type] ++;
					$this->totalesXversion ['5.5'] [$type] ++;
				} else if (strpos ( $data ['message'], '5.4' ) !== false) {
					$this->totalesXversion ['5.4'] [$type] ++;
					$this->totalesXversion ['5.5'] [$type] ++;
				} else if (strpos ( $data ['message'], '5.5' ) !== false) {
					$this->totalesXversion ['5.5'] [$type] ++;
				} else {
					$this->totalesXversion ['5.3'] [$type] ++;
					$this->totalesXversion ['5.4'] [$type] ++;
					$this->totalesXversion ['5.5'] [$type] ++;
				}
			}
		} else {
			return false;
		}
		fclose ( $handle );
		unset ( $handle );
		return $this->totalesXversion;
	}
}
