<?php

namespace iwaystatistics\crap;

class Parser {
	public function parse($filename) {
		if (file_exists ( $filename )) {
			
			$oXMLParser = simplexml_load_file ( $filename );
			$data = array (
					'method_count' => ( int ) $oXMLParser->stats->methodCount,
					'crap_method_count' => ( int ) $oXMLParser->stats->crapMethodCount,
					'crap_load' => ( int ) $oXMLParser->stats->crapLoad,
					'total_crap' => ( int ) $oXMLParser->stats->totalCrap,
					'crap_method_percent' => $oXMLParser->stats->crapMethodPercent
			);
		} else {
			$data = array (
					'method_count' => 0,
					'crap_method_count' => 0,
					'crap_load' =>  0,
					'total_crap' =>  0,
					'crap_method_percent' => 0
			);
		}
		return $data;
	}
}
