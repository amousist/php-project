<?php
namespace iwaystatistics\csniffer;
use iwaystatistics\base\AbstractDatabasePersister;
require_once (__DIR__.'/../base/AbstractDatabasePersister.php');

class DatabasePersister extends AbstractDatabasePersister{
	public function persist(array $data) {
		extract ( $data );
		
		$sql = <<<EOQ
INSERT INTO codigostandards (execution_id, total_errores, total_warnings)
VALUES (?, ?, ?)
EOQ;
		$stmt = $this->db->prepare ( $sql );
		$stmt->bind_param ( "iii", $executionId, $data ['errors'], $data ['warnings']);
		$stmt->execute ();
		$stmt->close ();
	}
}
