<?php

namespace iwaystatistics\phpcompat;

use iwaystatistics\base\AbstractDatabasePersister;
require_once (__DIR__ . '/../base/AbstractDatabasePersister.php');
class DatabasePersister extends AbstractDatabasePersister {
	public function persist(array $data) {
		extract ( $data );
		
		$sql = <<<EOQ
INSERT INTO phpversion (execution_id, php_version,total_warnings, total_errores)
VALUES (?, ?, ?, ?)
EOQ;
		$stmt = $this->db->prepare ( $sql );
		foreach ( $data as $version => $info ) {
			$stmt->bind_param ( "isii", $executionId, $version, $info ['warnings'], $info ['errors'] );
			$stmt->execute ();
		}
		$stmt->close ();
	}
}
