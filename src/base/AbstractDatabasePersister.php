<?php
namespace iwaystatistics\base;

abstract class AbstractDatabasePersister{
	
    protected $db = null;
	
	public function setDb(\Mysqli $db)
	{
		$this->db = $db;
		return $this;
	}
	
	public abstract function persist(array $data);
}